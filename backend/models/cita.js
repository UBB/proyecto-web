'use strict'
const mongoose = require('mongoose');
const CitaSchema = mongoose.Schema({
    rut: {
        type: String,
        required: true
    },
    // La especialista viene de la disponibilidad, es redundante aquí.
    /*especialista: {
        type: mongoose.Schema.ObjectId, ref: "especialista",
        required: true
    },*/
    disponibilidad: {
        type: mongoose.Schema.ObjectId, ref: "disponibilidad",
        required:  true
    }
});

module.exports = mongoose.model('cita', CitaSchema);
