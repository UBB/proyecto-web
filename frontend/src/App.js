import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import './App.css';
import IntegrantesComponent from './views/IntegrantesComponent';
import SignInComponent from './views/SignInComponent';
import NavbarComponent from './views/NavbarComponent';
import HorarioComponent from './views/HorarioComponent';
import AgendarComponent from './views/Agendar/AgendarComponent';
import AgendarHorarioComponent from './views/Agendar/AgendarHorarioComponent';
import AgendarConfirmarComponent from './views/Agendar/AgendarConfirmarComponent';
import BienvenidoComponent from './views/BienvenidoComponent';

function App() {
  return (
    <BrowserRouter>
      <NavbarComponent />
      <Routes>
        <Route exact path="/" element={<Navigate replace to="/bienvenido" />} />

        <Route path="/bienvenido" element={<BienvenidoComponent />} />

        <Route path="/integrantes" element={<IntegrantesComponent />} />

        <Route path="/agendar" element={<AgendarComponent />} />
        <Route path="/agendar/horario" element={<AgendarHorarioComponent />} />
        <Route path="/agendar/confirmar" element={<AgendarConfirmarComponent />} />

        <Route path="/admin" element={<SignInComponent />} />
        <Route path="/admin/horario" element={<HorarioComponent />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
